# Custom aliases
alias update="sudo apt update && sudo apt dist-upgrade && sudo apt autoremove && wget -O ~/.dotfiles/bin/nvim https://github.com/neovim/neovim/releases/latest/download/nvim.appimage && chmod u+x ~/.dotfiles/bin/nvim && cd && ~/.dotfiles/composer && composer update && cd -"
alias v="nvim"
# Smartcase by default.
alias rg="rg -S"
alias m="make"
alias b="basin"
alias d="ddev drush"
alias drush="basin drush"
alias dv="ddev"
alias vv="ddev exec nvim"
alias c="ddev composer"
alias dk="docker"
alias dkps="docker ps"
alias dkc="docker compose"
alias dkcu="docker compose up"
alias dke="docker exec -it"
alias dkce="docker compose exec"
alias t="tmux"
alias getmusic="rsync --archive --verbose --human-readable --delete --copy-links root@192.168.11.2:/media/kodama/data/CurrentMusic/ ~/Music/"

# uses SSHFS to mount a remote dir - which should mostly survive breaks in connection. mostly.
# sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3,IdentityFile=/home/me/.ssh/id_rsa user@server:/home/user/dir dirshare/
