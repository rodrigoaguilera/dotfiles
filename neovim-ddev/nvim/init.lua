require("config.lazy")

-- Add the g flag to search/replace by default.
vim.opt.gdefault = true
-- Ignore the capitalization when typing all lowercase.
vim.opt.ignorecase = true
-- Do not ignore capitals in searches
vim.opt.smartcase = true

-- NetRW
-- Human-readable file sizes.
vim.g.netrw_sizestyle = 'H'
-- Hide dotfiles. Comma-separated patterns. Files ignored by git.
-- Use lua [[ string notation to avoid escaping.
vim.g.netrw_list_hide = (vim.fn["netrw_gitignore#Hide"]()) .. [[,\(^\|\s\s\)\zs\.\S\+]]
-- Hide dotfiles by default. Toggle this using 'a'.
vim.g.netrw_hide = 1

vim.api.nvim_set_keymap('', '<leader>w', ':w<CR>', {
	desc = 'Write to file'
})
vim.api.nvim_set_keymap('n', '<leader>e', ':Explore<CR>', {
	desc = 'Open file explorer'
})
vim.api.nvim_set_keymap('n', '<leader>q', ':bd<CR>', {
	desc = 'Close a buffer'
})
vim.api.nvim_set_keymap('n', '<leader>h', ':bp<CR>', {
	desc = 'Previous buffer'
})
vim.api.nvim_set_keymap('n', '<leader>l', ':bn<CR>', {
	desc = 'Next buffer'
})

vim.api.nvim_set_keymap('n', '<leader>dc', [[:!drush cache:rebuild<CR>]]
, {
  desc = 'Rebuild Drupal cache',
  silent = true
})
