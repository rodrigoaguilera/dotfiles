return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  config = function ()
    local configs = require("nvim-treesitter.configs")
    configs.setup({
      ensure_installed = {
        "c",
        "html",
        "javascript",
        "json",
        "lua",
        'markdown',
        "php",
        "query",
        "twig",
        "vim",
        "vimdoc",
        "yaml",
      },
      -- Needed for installing parser syncronously from the Dockerfile.
      sync_install = true,
      highlight = { enable = true },
      indent = { enable = true },
    })
  end
}
