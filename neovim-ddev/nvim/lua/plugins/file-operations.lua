return {
  "chrisgrieser/nvim-genghis",
  dependencies = "stevearc/dressing.nvim",
  init = function()
    -- Init required due to https://github.com/chrisgrieser/nvim-genghis/issues/51
    require('genghis')
  end,
}
