return {
  -- Linting/fixing.
	'dense-analysis/ale',
  config = function()
    -- Configure fixer for the ALEFix function.
    vim.g.ale_fixers = {
      ["*"] = {'remove_trailing_lines', 'trim_whitespace'},
      php = {'phpcbf'}
    }
    -- Configure linters.
    vim.g.ale_linters_ignore = {
      ["twig"] = {'eslint'}
    }

    -- Keymaps
    vim.api.nvim_set_keymap('n', '<C-k>', '<Plug>(ale_previous_wrap)', {
      desc = 'Previous ale error',
      silent = true
    })
    vim.api.nvim_set_keymap('n', '<C-j>', '<Plug>(ale_next_wrap)', {
      desc = 'Next ale error',
      silent = true
    })


  end
}
