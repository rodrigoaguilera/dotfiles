return {
  -- Git line decorations
  'lewis6991/gitsigns.nvim',
  config = function()
    require('gitsigns').setup()
  end
}
