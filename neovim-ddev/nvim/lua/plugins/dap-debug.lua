return {
  'rcarriga/nvim-dap-ui',
  dependencies = {
    'mfussenegger/nvim-dap',
    'nvim-neotest/nvim-nio'
  },
  config = function ()
    local dap = require('dap')

    dap.adapters.php = {
      type = 'executable',
      command = 'node',
      args = { os.getenv('HOME') .. '/vscode-php-debug/out/phpDebug.js' }
    }
    dap.configurations.php = {
      {
        type = 'php',
        request = 'launch',
        name = 'Listen for Xdebug',
        port = 9003,
      }
    }
  end
}
