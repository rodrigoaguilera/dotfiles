require('plugins')

-- Set completeopt to have a better completion experience. suggested by the readme of nvim-cmp
vim.opt.completeopt="menu,menuone,noselect"
-- Add the g flag to search/replace by default.
vim.opt.gdefault = true
-- Ignore the capitalization when typing all lowercase.
vim.opt.ignorecase = true
-- Do not ignore capitals in searches
vim.opt.smartcase = true

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require('cmp')
local luasnip = require('luasnip')

cmp.setup({
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-b>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
	},
	{
		{ name = 'buffer' },
	})
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
	mapping = cmp.mapping.preset.cmdline(),
	sources = {
		{ name = 'buffer' }
	}
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = 'path' }
	}, {
		{ name = 'cmdline' }
	})
})
-- Set up lspconfig. Deprecated. Setup in lspconfig config key.
-- local capabilities = require('cmp_nvim_lsp').default_capabilities()
-- require('lspconfig').phpactor.setup {
-- 	capabilities = capabilities
-- }
vim.keymap.set('n','<Leader>ca', vim.lsp.buf.code_action, {})

-- Load the rafamadriz/friendly-snippets collection.
require("luasnip.loaders.from_vscode").lazy_load()

local dap = require('dap')

dap.adapters.php = {
  type = 'executable',
  command = 'node',
  args = { os.getenv('HOME') .. '/Projects/vscode-php-debug/out/phpDebug.js' }
}

-- Use docker4drupal php containers path.
dap.configurations.php = {
  {
    type = 'php',
    request = 'launch',
    name = 'Listen for Xdebug',
    port = 9003,
    pathMappings = {
      ["/var/www/html"] = "${workspaceFolder}",
    }
  }
}

-- Theme files inside Drupal themes should be treated as PHP. Not needed after nvim 0.10.
local augroup = vim.api.nvim_create_augroup('drupal_theme_ft', {clear = true})
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {
  pattern = '*.theme',
  group = augroup,
  command = 'setf php'
})

-- Configure fixer for the ALEFix function.
vim.g.ale_fixers = {
	["*"] = {'remove_trailing_lines', 'trim_whitespace'},
	php = {'phpcbf'}
}
-- Configure linters.
vim.g.ale_linters_ignore = {
	["twig"] = {'eslint'}
}

-- NetRW
-- Human-readable file sizes.
vim.g.netrw_sizestyle = 'H'
-- Hide dotfiles. Comma-separated patterns. Files ignored by git.
-- Use lua [[ string notation to avoid escaping.
vim.g.netrw_list_hide = (vim.fn["netrw_gitignore#Hide"]()) .. [[,\(^\|\s\s\)\zs\.\S\+]]
-- Hide dotfiles by default. Toggle this using 'a'.
vim.g.netrw_hide = 1

-- MAPPINGS
vim.api.nvim_set_keymap('', ' ', '\\',{
	noremap = false,
	desc = 'Space as leader'
})

vim.api.nvim_set_keymap('', '<leader>w', ':w<CR>', {
	desc = 'Write to file'
})
vim.api.nvim_set_keymap('n', '<leader>e', ':Explore<CR>', {
	desc = 'Open file explorer'
})
vim.api.nvim_set_keymap('n', '<leader>q', ':bd<CR>', {
	desc = 'Close a buffer'
})
vim.api.nvim_set_keymap('n', '<leader>h', ':bp<CR>', {
	desc = 'Previous buffer'
})
vim.api.nvim_set_keymap('n', '<leader>l', ':bn<CR>', {
	desc = 'Next buffer'
})

vim.api.nvim_set_keymap('n', '<C-k>', '<Plug>(ale_previous_wrap)', {
	desc = 'Previous ale error',
	silent = true
})
vim.api.nvim_set_keymap('n', '<C-j>', '<Plug>(ale_next_wrap)', {
	desc = 'Next ale error',
	silent = true
})

-- Telescope keymaps
vim.keymap.set('n','<Leader>ff',function() return require('telescope.builtin').find_files() end)
vim.keymap.set('n','<Leader>fg',function() return require('telescope.builtin').live_grep() end)
vim.keymap.set('n','<Leader>fb',function() return require('telescope.builtin').buffers() end)
vim.keymap.set('n','<Leader>fh',function() return require('telescope.builtin').help_tags() end)

if vim.fn.isdirectory('.basin') == 1 then
	vim.api.nvim_set_keymap('n', '<leader>dc', [[:!basin drush cache:rebuild<CR>]]
		, {
		desc = 'Rebuild cache for basin',
		silent = true
	})
end
if vim.fn.isdirectory('.ddev') == 1 then
	vim.api.nvim_set_keymap('n', '<leader>dc', [[:!ddev drush cache:rebuild<CR>]]
		, {
		desc = 'Rebuild cache for basin',
		silent = true
	})
end
