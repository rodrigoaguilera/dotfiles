local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	-- Support more languages like twig. Disabled as it overwrites shiftwidth/indet set by editorconfig.
	-- 'sheerun/vim-polyglot',
	-- Autoclose braces with enter.
  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    config = true
    -- use opts = {} for passing setup options
    -- this is equivalent to setup({}) function
  },
	-- Linting/fixing.
	'dense-analysis/ale',
  -- UNIX helpers like :Rename. To be replaced by 'chrisgrieser/nvim-genghis' when nvim > 0.10
  'tpope/vim-eunuch',
  -- Use gcc and gc<motion> to comment lines.
  {
    'numToStr/Comment.nvim',
    opts = { },
    lazy = false,
  },
  -- Find files
  {
    'nvim-telescope/telescope.nvim', tag = '0.1.6',
		dependencies = { 'nvim-lua/plenary.nvim' }
  },
	-- Return to the last edit position.
  {
    "mrcjkb/nvim-lastplace",
    init = function()
      -- optional configuration
    end,
  },
  {
    'L3MON4D3/LuaSnip',
    -- follow latest release.
    version = "v2.3",
    -- install jsregexp (optional!).
    build = "make install_jsregexp"
  },
  'saadparwaiz1/cmp_luasnip',
  -- Snippet collection
  'rafamadriz/friendly-snippets',
  'andy-blum/drupal-smart-snippets',
  -- Autocompletion
  'hrsh7th/nvim-cmp',
  -- Sources for autocompletion
  {
    'neovim/nvim-lspconfig',
    config = function()
      local lspconfig = require('lspconfig')
      lspconfig.phpactor.setup({})
    end
  },
  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/cmp-buffer',
  'hrsh7th/cmp-path',
  'hrsh7th/cmp-cmdline',

	-- Git line decorations
	{
		'lewis6991/gitsigns.nvim',
    config = function()
        require('gitsigns').setup()
    end
	},
  {
    'kylechui/nvim-surround',
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    config = function()
        require("nvim-surround").setup({})
    end
  },
  { "rcarriga/nvim-dap-ui", dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"} }
})
