# Dotfiles

Personal configurations. feel free to copy any part of it

# Setup

To place the symlinks in their locations `stow`
is preferred. For example to install the tmux configuration:
```
stow tmux
```
