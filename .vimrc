" Since this script has multibyte characters this lines are needed.
set encoding=utf-8
scriptencoding utf-8

" Install vim-plug.
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  augroup vimrc
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  augroup END
endif

" Install plugins.
call plug#begin('~/.vim/plugged')
" Sensible defaults.
Plug 'tpope/vim-sensible'
" Read .editorconfig files and adjust configuration.
Plug 'editorconfig/editorconfig-vim'
" Linting/fixing.
Plug 'w0rp/ale'
" Support more languages like twig.
Plug 'sheerun/vim-polyglot'
" Use gcc and gc<motion> to comment lines.
Plug 'tpope/vim-commentary'
" Search the source code.
Plug 'mileszs/ack.vim'
" Autocompletion
Plug 'lifepillar/vim-mucomplete'
" Snippet manager.
Plug 'sirver/ultisnips'
" Collection of snippets.
Plug 'honza/vim-snippets'
" Perform web searches.
Plug 'linluk/vim-websearch'
" Add a motion for sorting
Plug 'christoomey/vim-sort-motion'
" Add mappings to quote text objects.
Plug 'tpope/vim-surround'
" Repeat commands done with surround.
Plug 'tpope/vim-repeat'
" UNIX helpers like :Rename.
Plug 'tpope/vim-eunuch'
" Clear the highlighted text after a search.
Plug 'junegunn/vim-slash'
" Show git changes in the gutter.
Plug 'airblade/vim-gitgutter'
" Guess the indent of files.
Plug 'tpope/vim-sleuth'
" Smooth scroll.
Plug 'psliwka/vim-smoothie'
" Nginx syntax.
Plug 'chr4/nginx.vim'
call plug#end()

" Use defaults.
if filereadable('/usr/share/vim/vim81/defaults.vim')
  source /usr/share/vim/vim81/defaults.vim
endif

let g:web_search_command = 'firefox'
let g:web_search_use_default_mapping = 'yes'

" Configure the ack vim plugin to use the silver searcher.
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Enable automatic completion at startup.
let g:mucomplete#enable_auto_at_startup = 1
" Default is tab but interferes with completion.
let g:UltiSnipsExpandTrigger='<C-j>'
let g:UltiSnipsJumpForwardTrigger = '<C-j>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'
" Autocomplete ultisnips.
call add(g:mucomplete#chains['default'], 'ulti')

" Set options.

" Show “invisible” characters.
set listchars=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list

" Required by vim-mucomplete.
set completeopt+=menuone
" Required by automatic completion for vim-mucomplete.
set completeopt+=noselect
set completeopt+=noinsert
" Shut off completion messages.
set shortmess+=c
" Vim beeps during completion. Supress that.
set belloff+=ctrlg

" Enable current line number.
set number
" Use relative line numbers.
set relativenumber

" Add the g flag to search/replace by default.
set gdefault

" Highlight searches.
set hlsearch

" Indent automatically.
set autoindent

" Does the right thing most of the time.
set smartindent

" Allow background buffers without saving.
set hidden

" Split to right by default
set splitright

" Ignore the capitalization when typing all lowercase.
set ignorecase
" Do not ignore capitals in searches
set smartcase

" Store swapfiles away from the code.
set directory=$HOME/.dotfiles/vimdata/swapfiles//


" NetRW
" Detail View.
let g:netrw_liststyle = 1
" Human-readable file sizes.
let g:netrw_sizestyle = 'H'
" Hide dotfiles.
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
" Hide dotfiles by default. Toggle this using 'a'.
let g:netrw_hide = 1
" Turn off banner
let g:netrw_banner = 0

" Mappings

" Space as a second mapleader so there is visual feedback in the status line.
map <Space> \

" Change working directory to the one the current file belongs to.
noremap <leader>cd :lcd %:p:h<CR>:pwd<CR>

" Write to file.
noremap <leader>w :w<CR>

" Explore in vertical split.
nnoremap <leader>e :Explore! <CR>

" Cycle through buffers.
nnoremap <C-H> :bp <CR>
nnoremap <C-L> :bn <CR>
" Close a buffer.
nnoremap <leader>q :bd <CR>

" Search the source code.
nnoremap <leader>a :Ack!<Space>

" Fix current file.
nnoremap <leader>f :ALEFix <CR>

" Navigate errors.
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

nnoremap <leader>d :call DrupalToggle()<CR>

" Autocommands.
augroup vimrc
  " Strip and trim trailing whitespace on save.
  autocmd BufWritePre * %s/\s\+$//e
augroup END

" Theme files inside Drupal themes should be treated as PHP.
augroup drupaltheme_ft
  au!
  autocmd BufNewFile,BufRead *.theme setf php
augroup END

" Configure fixer for the ALEFix function.
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'php': ['phpcbf'],
\   'yaml': ['prettier'],
\}

" ALE automatically tries to use /vendor/bin/phpcs but the setup has the
" global configured.
let g:ale_php_phpcs_use_global = 1
let g:ale_php_phpcbf_use_global = 1
let g:ale_php_phpcs_options='--extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml'
let g:ale_javascript_eslint_use_global = 1
let g:ale_scss_stylelint_use_global = 1
let g:ale_yaml_yamllint_options = '-d {extends: default, rules: {document-start: disable}}'
let g:ale_dockerfile_hadolint_use_docker = 'always'

augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.info.yml let b:ale_linter_aliases = ['php', 'yaml']
augroup END

" Avoid losing syntax on big files.
set redrawtime=10000

" ALE tries to check code too frequently.
let g:ale_lint_delay = 2000

" Set the Standard to Drupal. TODO: Write a proper plugin.
let g:ale_php_phpcs_standard = 'Drupal,DrupalPractice'
let g:ale_php_phpcbf_standard = 'Drupal,DrupalPractice'
function! DrupalToggle()
  if g:ale_php_phpcs_standard ==# 'Drupal,DrupalPractice'
    let g:ale_php_phpcs_standard = 'PSR12'
    let g:ale_php_phpcbf_standard = 'PSR12'
  else
    let g:ale_php_phpcs_standard = 'Drupal,DrupalPractice'
    let g:ale_php_phpcbf_standard = 'Drupal,DrupalPractice'
  endif
endfunction
